echo $SOURCEDIR
mkdir "$SOURCEDIR"
chmod -R 777 "$SOURCEDIR"

upper_name=""
upper_all_name=""
lower_name=""
lower_all_name=""

if [[ $name == *"-"* ]]; then
  IFS='-' # hyphen (-) is set as delimiter
  read -ra ADDR <<< "$name" # str is read into an array as tokens separated by IFS
  for i in "${ADDR[@]}"; do # access each element of array
    echo ${i}
    upper_tmp="$(tr '[:lower:]' '[:upper:]' <<< ${i:0:1})${i:1}"
    upper_all_tmp="$(tr '[:lower:]' '[:upper:]' <<< "$i")"
    upper_name=${upper_name}${upper_tmp}
    lower_tmp="$(tr '[:upper:]' '[:lower:]' <<< "$i")"

    if [ "$upper_all_name" == "" ]; then
      upper_all_name="${upper_all_name}${upper_all_tmp}"
    else
      upper_all_name="${upper_all_name}_${upper_all_tmp}"
    fi

    if [ "$lower_name" == "" ]; then
      lower_name="${lower_tmp}"
    else
      lower_name="${lower_name}${upper_tmp}"
    fi

    if [ "$lower_all_name" == "" ]; then
      lower_all_name="${lower_tmp}"
    else
      lower_all_name="${lower_all_name}_${lower_tmp}"
    fi
    
  done
  IFS=' ' #
else
  upper_name="$(tr '[:lower:]' '[:upper:]' <<< ${name:0:1})${name:1}"
  upper_all_name="$(tr '[:lower:]' '[:upper:]' <<< "$name")"
  lower_name="$(tr '[:upper:]' '[:lower:]' <<< ${name:0:1})${name:1}"
  lower_all_name="$(tr '[:upper:]' '[:lower:]' <<< "$name")"
fi