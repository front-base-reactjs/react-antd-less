touch "$SOURCEDIR/utils/$name-detail.utils.ts"
echo "
${COPPYRIGHT}
import { Form, FormInstance } from 'antd';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { routePath } from 'routing/path.routing';
import { DataResponseError, ResponseCode } from 'utils/api/api.types';
import { apiCreate${upper_name}, apiGetDetail${upper_name}, apiUpdate${upper_name} } from './${name}.services';
import { IBody${upper_name}Res } from './${name}.types';

interface Utils {
  form: FormInstance<any>;
  loadingCreateEdit: boolean;

  loadingDetail: boolean;

  ${lower_name}Id?: number;
  onSaveData: () => void;
  onCancelSaveData: () => void;
}

export default function ${upper_name}DetailUtils(): Utils {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [loadingCreateEdit, setLoadingCreateEdit] = useState<boolean>(false);

  const [loadingDetail, setLoadingDetail] = useState<boolean>(false);

  const [${lower_name}Id, set${upper_name}Id] = useState<number>();

  const params = useParams();

  useEffect(() => {
    const get${upper_name}Id = params?.id;
    if (get${upper_name}Id) {
      set${upper_name}Id(Number(get${upper_name}Id));
      fetchDetail${upper_name}(Number(get${upper_name}Id));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  // fetch detail ${name}
  const fetchDetail${upper_name} = async (id: number) => {
    try {
      if (loadingDetail) return;
      setLoadingDetail(true);
      const res = await apiGetDetail${upper_name}(id);
      setLoadingDetail(false);
      if (res.code === ResponseCode.SUCCESS) {
        form.setFieldsValue({
          name: res?.content?.name || '',
        });
      }
    } catch (error) {
      setLoadingDetail(false);
      throw error;
    }
  };

  const onSaveData = () => {
    if (loadingCreateEdit) return;
    form.validateFields().then(async (values) => {
      try {
        const body: IBody${upper_name}Res = {
          name: values?.name,
        };

        // check if id valid is edit
        if (${lower_name}Id) {
          setLoadingCreateEdit(true);
          const res = await apiUpdate${upper_name}(${lower_name}Id, body);
          setLoadingCreateEdit(false);
          if (res.code === ResponseCode.SUCCESS) {
            navigate(routePath.${upper_name});
          }
        } else {
          setLoadingCreateEdit(true);
          const res = await apiCreate${upper_name}(body);
          setLoadingCreateEdit(false);
          if (res.code === ResponseCode.SUCCESS) {
            navigate(routePath.${upper_name});
          }
        }
      } catch (error: any) {
        const newError = error as DataResponseError;
        setLoadingCreateEdit(false);
        throw newError;
      }
    });
  };

  const onCancelSaveData = () => {
    navigate(-1);
  };

  return {
    form,
    loadingCreateEdit,

    loadingDetail,

    ${lower_name}Id,
    onSaveData,
    onCancelSaveData
  };
}

" >> "$SOURCEDIR/utils/$name-detail.utils.ts"
echo "=============> Created $SOURCEDIR/utils/$name-detail.utils.ts"