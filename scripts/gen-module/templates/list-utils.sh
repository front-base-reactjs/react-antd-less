touch "$SOURCEDIR/utils/$name.utils.ts"
echo "
${COPPYRIGHT}
import { apiGetAll${upper_name}, apiRemove${upper_name} } from './${name}.services';
import { PAGINATE_DEFAULT } from 'constants/common.constants';
import { useEffect, useState } from 'react';
import { ICommonParamsReq } from 'types/common.types';
import { I${upper_name}, I${upper_name}Params } from './${name}.types';
import { ResponseCode } from 'utils/api/api.types';
import { Form, FormInstance } from 'antd';

interface Utils {
  // DELETE ITEM
  showModalConfirm: boolean;

  loadingList: boolean;
  data: I${upper_name}[];
  params: ICommonParamsReq;
  total: number;
  formFilter: FormInstance<any>;
  onSubmitFilter: () => void;
  onShowModalConfirmDelete: (item: I${upper_name}) => void;
  onChangePage: (page: number) => void;
  onSubmitDeleteItem: () => void;
  onCancelModalConfirm: () => void;
}

export default function ${upper_name}Utils(): Utils {
  const [loadingList, setLoadingList] = useState<boolean>(false);
  const [data, setData] = useState<I${upper_name}[]>([]);
  const [total, setTotal] = useState<number>(0);
  const [formFilter] = Form.useForm();
  const [params, setParams] = useState<I${upper_name}Params>({
    limit: PAGINATE_DEFAULT.LIMIT,
    page: PAGINATE_DEFAULT.PAGE
  });

  useEffect(() => {
    fetchList${upper_name}(params);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  // fetch list ${name}
  const fetchList${upper_name} = async (params: I${upper_name}Params) => {
    try {
      if (loadingList) return;
      setLoadingList(true);
      const res = await apiGetAll${upper_name}(params);
      setLoadingList(false);
      if (res.code === ResponseCode.SUCCESS) {
        setData(res?.content || []);
        setTotal(res?.metadata?.total);
      }
    } catch (error) {
      setLoadingList(false);
    }
  };

  // on change page
  const onChangePage = (page: number) => {
    setParams({
      ...params,
      page
    });
  };

  // filter ${name}
  const onSubmitFilter = () => {
    formFilter.validateFields().then(async (values) => {
      setParams({
        ...params,
        name: values?.name ? values?.name.trim() : undefined,
      });
    });
  }

  // DELETE ITEM
  const [showModalConfirm, setShowModalConfirm] = useState<boolean>(false);
  const [itemNeedDelete, setItemNeedDelete] = useState<I${upper_name}>();
  const [loadingDelete, setLoadingDelete] = useState<boolean>(false);

  // on click show modal confirm delete item
  const onShowModalConfirmDelete = (item: I${upper_name}) => {
    setItemNeedDelete(item);
    setShowModalConfirm(true);
  };

  // confirm delete item
  const onSubmitDeleteItem = async () => {
    if (!itemNeedDelete || loadingDelete) return;
    try {
      setLoadingDelete(true);
      const res = await apiRemove${upper_name}(itemNeedDelete.id);
      setLoadingDelete(false);
      if (res.code === ResponseCode.SUCCESS) {
        setParams({ ...params, page: 1 });
        setShowModalConfirm(false);
        setItemNeedDelete(undefined);
      }
    } catch (error) {
      setLoadingDelete(false);
      setShowModalConfirm(false);
      setItemNeedDelete(undefined);
      throw error;
    }
  };

  const onCancelModalConfirm = () => {
    setShowModalConfirm(false);
    setItemNeedDelete(undefined);
  };

  return {
    showModalConfirm,
    onSubmitDeleteItem,
    onShowModalConfirmDelete,
    onCancelModalConfirm,

    loadingList,
    data,
    params,
    total,
    formFilter,
    onChangePage,
    onSubmitFilter
  };
}

" >> "$SOURCEDIR/utils/$name.utils.ts"
echo "=============> Created $SOURCEDIR/utils/$name.utils.ts"