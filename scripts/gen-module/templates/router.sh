touch "src/routing/routes/$name.route.ts"
echo "
${COPPYRIGHT}
import { routePath } from 'routing/path.routing'
import { lazy } from 'react';
import { HomeOutlined } from '@ant-design/icons';
import React from 'react';
import { IRouter } from 'routing/config.routing';

export const ${upper_name}Route: IRouter[] = [
  {
    path: routePath.${upper_name},
    element: lazy(() => import('modules/${name}/${name}.page')),
    icons: React.createElement(HomeOutlined),
    name: '${upper_name}'
  },
  {
    path: \`\${routePath.${upper_name}}/:id\`,
    element: lazy(() => import('modules/${name}/${name}-detail.page')),
    name: '${upper_name} detail',
    hiddenMenu: true
  },
  {
    path: routePath.${upper_name}Add,
    element: lazy(() => import('modules/${name}/${name}-detail.page')),
    name: '${upper_name} new',
    hiddenMenu: true
  },
]

" >> "src/routing/routes/$name.route.ts"
echo "=============> Created src/routing/routes/$name.route.ts"