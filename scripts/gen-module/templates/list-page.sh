touch "$SOURCEDIR/$name.page.tsx"
echo "
${COPPYRIGHT}
import { Card, Col, Form, Row, Table } from 'antd';
import ButtonForm from 'libraries/form/button/button-form';
import ContainerLayout from 'libraries/layouts/container.layout';
import { useTranslation } from 'react-i18next';
import { PlusOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import { routePath } from 'routing/path.routing';
import Loading from 'libraries/components/loading';
import PaginationBase from 'libraries/components/pagination-base';
import ModalConfirm from 'libraries/models/modal-confirm';
import InputForm from 'libraries/form/input/input-form';

import ${upper_name}Columns from './utils/${name}.columns';
import ${upper_name}Utils from './utils/${name}.utils';

export default function ${upper_name}Page() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const {
    loadingList,
    data,
    params,
    total,
    formFilter,
    onChangePage,
    onSubmitFilter,

    // DELETE ITEM
    showModalConfirm,
    onShowModalConfirmDelete,
    onSubmitDeleteItem,
    onCancelModalConfirm
  } = ${upper_name}Utils();

  return (
    <ContainerLayout
      title={t('${name}_page.title')}
      actions={
        <ButtonForm
          type='default'
          title={t('add')}
          icon={<PlusOutlined />}
          onClick={() => navigate(routePath.${upper_name}Add)}
        />
      }
      filterActions={
        <Form layout='vertical' name='basic' autoComplete='off' form={formFilter}>
          <Row gutter={30}>
            <Col span={6}>
              <InputForm
                label={t('${lower_all_name}_page.name')}
                name='name'
                allowClear
              />
            </Col>
            <Col span={6}>
              <div style={{ marginBottom: 30 }}></div>
              <ButtonForm
                htmlType='submit'
                title={t('filter')}
                onClick={onSubmitFilter}
              />
            </Col>
          </Row>
        </Form>
      }
    >
      <Card>
        <Row>
          <Col span={24}>
            <Table
              loading={loadingList ? { indicator: <Loading /> } : false}
              pagination={false}
              dataSource={data}
              columns={${upper_name}Columns({
                page: params.page,
                onConFirmDelete: onShowModalConfirmDelete,
                pathEdit: routePath.${upper_name}
              })}
              rowKey='id'
            />
          </Col>

          {/** Pagination */}
          {total > params.limit && (
            <PaginationBase page={params.page} total={total} onPageChange={onChangePage} />
          )}
        </Row>
      </Card>

      {/** Modal Confirm Delete */}
      <ModalConfirm
        visible={showModalConfirm}
        onConfirmOk={onSubmitDeleteItem}
        onCancel={onCancelModalConfirm}
      />
    </ContainerLayout>
  );
};


" >> "$SOURCEDIR/$name.page.tsx"
echo "=============> Created $SOURCEDIR/$name.page.tsx"