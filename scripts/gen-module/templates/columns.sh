touch "$SOURCEDIR/utils/$name.columns.tsx"
echo "
${COPPYRIGHT}
import { Typography } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { PAGINATE_DEFAULT } from 'constants/common.constants';
import ActionsButton from 'libraries/form/button/actions-button';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { I${upper_name} } from './${name}.types';

interface ColumnParams {
  page: number;
  pathEdit?: string;

  onConFirmDelete: (item: I${upper_name}) => void;
}

export default function ${upper_name}Columns({
  page,
  pathEdit,

  onConFirmDelete
}: ColumnParams): ColumnsType<I${upper_name}> {
  const { t } = useTranslation();
  const navigate = useNavigate();

  const onHandleEdit = (item: I${upper_name}) => {
    if (pathEdit) return navigate(\`\${pathEdit}/\${item.id}\`);
  };

 return [
    {
      title: t('stt'),
      dataIndex: 'index',
      key: 'index',
      width: '5%',
      render: (_: number, __: I${upper_name}, index: number) => {
        return (
          <span>
            {Number(page) > 1
              ? (Number(page) - 1) * PAGINATE_DEFAULT.LIMIT + (index + 1)
              : index + 1}
          </span>
        );
      }
    },
    {
      title: t('${lower_all_name}_page.name'),
      dataIndex: 'name',
      key: 'name',
      width: '20%',
      render: (name: string) => {
        return <Typography.Text>{name}</Typography.Text>;
      }
    },
    {
      title: t('action'),
      dataIndex: 'action',
      key: 'action',
      width: '5%',
      render: (_: string, item: I${upper_name}) => {
        return (
          <ActionsButton
            onEdit={() => onHandleEdit(item)}
            onConfirmDelete={() => onConFirmDelete(item)}
          />
        );
      }
    }
  ];
};

" >> "$SOURCEDIR/utils/$name.columns.tsx"
echo "=============> Created $SOURCEDIR/utils/$name.columns.tsx"