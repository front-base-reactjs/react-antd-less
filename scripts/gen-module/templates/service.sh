touch "$SOURCEDIR/utils/$name.services.ts"
echo "
${COPPYRIGHT}
import { ResponseBase } from 'utils/api/api.types';
import ApiUtils from 'utils/api/api.utils';
import { IBody${upper_name}Res, I${upper_name}, I${upper_name}Params } from './${name}.types';

const apiName = {
  ${lower_name}_list: '/${name}/list',
  ${lower_name}: '/${name}'
};

export const apiGetAll${upper_name} = async (params?: I${upper_name}Params) => {
  const res = await ApiUtils.fetch<I${upper_name}Params, ResponseBase<I${upper_name}[]>>(
    apiName.${lower_name}_list,
    params
  );
  return res;
};

export const apiGetDetail${upper_name} = async (id: number) => {
  const res = await ApiUtils.fetch<number, ResponseBase<I${upper_name}>>(\`\${apiName.${lower_name}}/\${id}\`);
  return res;
};

export const apiRemove${upper_name} = async (id: number) => {
  const res = await ApiUtils.remove<number, ResponseBase<string>>(\`\${apiName.${lower_name}}/\${id}\`);
  return res;
};

export const apiUpdate${upper_name} = async (id: number, body: IBody${upper_name}Res) => {
  const res = await ApiUtils.put<IBody${upper_name}Res, ResponseBase<string>>(
    \`\${apiName.${lower_name}}/\${id}\`,
    body
  );
  return res;
};

export const apiCreate${upper_name} = async (body: IBody${upper_name}Res) => {
  const res = await ApiUtils.post<IBody${upper_name}Res, ResponseBase<I${upper_name}>>(apiName.${lower_name}, body);
  return res;
};

" >> "$SOURCEDIR/utils/$name.services.ts"
echo "=============> Created $SOURCEDIR/utils/$name.services.ts"