touch "$SOURCEDIR/utils/$name.types.ts"
echo "
${COPPYRIGHT}
import { ICommonParamsReq } from 'types/common.types';

export interface I${upper_name}Params extends ICommonParamsReq {
  name?: string;
}

export interface I${upper_name} {
  id: number;
  name: string;
}

export interface IBody${upper_name}Res {
  name: string;
}

" >> "$SOURCEDIR/utils/$name.types.ts"
echo "=============> Created $SOURCEDIR/utils/$name.types.ts"