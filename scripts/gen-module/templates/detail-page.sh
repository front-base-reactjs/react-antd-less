touch "$SOURCEDIR/$name-detail.page.tsx"
echo "
${COPPYRIGHT}
import { Card, Col, Form, Row } from 'antd';
import ButtonForm from 'libraries/form/button/button-form';
import InputForm from 'libraries/form/input/input-form';
import ContainerLayout from 'libraries/layouts/container.layout';
import { useTranslation } from 'react-i18next';
import ${upper_name}DetailUtils from './utils/${name}-detail.utils';

export default function ${upper_name}DetailPage() {
  const { t } = useTranslation();
  const {
    form,
    loadingCreateEdit,
    loadingDetail,

    ${lower_name}Id,
    onSaveData,
    onCancelSaveData
  } = ${upper_name}DetailUtils();
  return (
    <ContainerLayout
      title={${lower_name}Id ? t('${lower_all_name}_page.title_edit') : t('${lower_all_name}_page.title_add')}
      titlePage={${lower_name}Id ? t('${lower_all_name}_page.title_edit') : t('${lower_all_name}_page.title_add')}
      loading={loadingDetail}
    >
      <Form layout='vertical' name='basic' autoComplete='off' form={form}>
        <Card>
          <Row gutter={50}>
            <Col span={12}>
              <InputForm
                label={t('${lower_all_name}_page.name')}
                name='name'
                rules={[
                  {
                    required: true,
                    message: t('messages.errors.require', { field: t('${lower_all_name}_page.name') })
                  }
                ]}
              />
            </Col>
          </Row>
        </Card>

        <Card style={{ marginTop: 32 }}>
          <ButtonForm
            type='default'
            title={t('cancel')}
            className='mr-12'
            onClick={onCancelSaveData}
          />
          <ButtonForm
            htmlType='submit'
            title={t('save')}
            loading={loadingCreateEdit}
            onClick={onSaveData}
          />
        </Card>
      </Form>
    </ContainerLayout>
  );
}

" >> "$SOURCEDIR/$name-detail.page.tsx"
echo "=============> Created $SOURCEDIR/$name-detail.page.tsx"