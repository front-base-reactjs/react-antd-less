#!/bin/bash

clear
NOW=$(date +"%m-%d-%Y %H:%M:%S")
echo 'Start Module'
echo -e "Please enter your module: "
read name
echo -e "Please enter your author name: "
read AUTHOR

AUTHORNAME="vungbt1999"
[ ! -z "$AUTHOR" ] && AUTHORNAME="$AUTHOR"

COPPYRIGHT="/* 
  Created by ${AUTHORNAME} at ${NOW}
  Module ${name}
*/"

SOURCEDIR="src/modules/$name" #

if [ -d $SOURCEDIR ] 
then
echo "Directory $SOURCEDIR exists." 
else
source $(dirname $0)/variables.sh

# Generate list-screen
source $(dirname $0)/templates/list-page.sh

# Generate detail-screen
source $(dirname $0)/templates/detail-page.sh

# generate utils folder
# Generate types
mkdir "$SOURCEDIR/utils"
source $(dirname $0)/templates/types.sh

# Generate service
source $(dirname $0)/templates/service.sh

# Generate detail-utils
source $(dirname $0)/templates/detail-utils.sh

# Generate columns
source $(dirname $0)/templates/columns.sh

# Generate list-utils
source $(dirname $0)/templates/list-utils.sh

# Generate router
source $(dirname $0)/templates/router.sh
ROUTER_PATH="src/routing/config.routing.ts"

case "$OSTYPE" in
  msys* | cygwin*)
    # insert path router
    sed -i -e "/contact\/add/a \ ${upper_name}: '/${name}'," "src/routing/path.routing.ts"
    sed -i -e "/contact\/add/a \ ${upper_name}Add: '/${name}/add'," "src/routing/path.routing.ts"

    # insert router
    sed -i -e "/contact\.route/a \import { ${upper_name}Route } from './routes/${name}.route';" $ROUTER_PATH
    sed -i -e "/\.\.\.\ContactRoute\,/a \     ...${upper_name}Route," $ROUTER_PATH
    ;;
  *)
    # insert path router
      sed -i '' '/contact\/add/i\'$'\n'"\  ${upper_name}: '/${name}',"$'\n' "src/routing/path.routing.ts"
      sed -i '' '/contact\/add/i\'$'\n'"\  ${upper_name}Add: '/${name}/add',"$'\n' "src/routing/path.routing.ts"

    # insert router
      sed -i '' '/contact\.route/i\'$'\n'"\import { ${upper_name}Route } from './routes/${name}.route';"$'\n' $ROUTER_PATH
      sed -i '' '/\.\.\.\ContactRoute\,/i\'$'\n'"\      ...${upper_name}Route,"$'\n' $ROUTER_PATH
    ;;
esac

echo "End Scripts"
fi