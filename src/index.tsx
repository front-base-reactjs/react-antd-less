import React from 'react';
import ReactDOM from 'react-dom';
// add redux store
import { Provider } from 'react-redux';
// router config
import { BrowserRouter } from 'react-router-dom';
// add locale config
import 'utils/i18n';
// import UI
import 'themes/styles/index.less';
import 'styles/global.less';

import store from 'utils/redux-store';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
