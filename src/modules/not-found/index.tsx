import ContainerLayout from 'libraries/layouts/container.layout';
import React from 'react';

export default function NotFoundPage() {
  return <ContainerLayout>NotFoundPage</ContainerLayout>;
}
