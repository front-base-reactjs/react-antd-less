import { Typography } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { PAGINATE_DEFAULT } from 'constants/common.constants';
import ActionsButton from 'libraries/form/button/actions-button';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { IContact } from './contact.types';

interface ColumnParams {
  page: number;
  pathEdit?: string;

  onConFirmDelete: (item: IContact) => void;
}

export default function ContactColumns({
  page,
  pathEdit,

  onConFirmDelete
}: ColumnParams): ColumnsType<IContact> {
  const { t } = useTranslation();
  const navigate = useNavigate();

  const onHandleEdit = (item: IContact) => {
    if (pathEdit) return navigate(`${pathEdit}/${item.id}`);
  };

  return [
    {
      title: t('stt'),
      dataIndex: 'index',
      key: 'index',
      width: '5%',
      render: (_: number, __: IContact, index: number) => {
        return (
          <span>
            {Number(page) > 1
              ? (Number(page) - 1) * PAGINATE_DEFAULT.LIMIT + (index + 1)
              : index + 1}
          </span>
        );
      }
    },
    {
      title: t('contact_page.name'),
      dataIndex: 'name',
      key: 'name',
      width: '20%',
      render: (name: string) => {
        return <Typography.Text>{name}</Typography.Text>;
      }
    },
    {
      title: t('column_label.phone'),
      dataIndex: 'phone',
      key: 'phone',
      width: '20%',
      render: (phone: string) => {
        return <Typography.Text>{phone}</Typography.Text>;
      }
    },
    {
      title: t('column_label.address'),
      dataIndex: 'address',
      key: 'address',
      width: '20%',
      render: (address: string) => {
        return <Typography.Text>{address}</Typography.Text>;
      }
    },
    {
      title: t('action'),
      dataIndex: 'action',
      key: 'action',
      width: '5%',
      render: (_: string, item: IContact) => {
        return (
          <ActionsButton
            onEdit={() => onHandleEdit(item)}
            onConfirmDelete={() => onConFirmDelete(item)}
          />
        );
      }
    }
  ];
}
