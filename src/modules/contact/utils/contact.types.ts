import { ICommonParamsReq } from 'types/common.types';

export interface IContactParams extends ICommonParamsReq {
  name?: string;
  phone?: string;
}

export interface IContact {
  id: number;
  name: string;
  phone: string;
  address: string;
}

export interface IBodyContactRes {
  name: string;
  phone: string;
  address: string;
}
