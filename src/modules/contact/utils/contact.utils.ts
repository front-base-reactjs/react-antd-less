import { apiGetAllContact, apiRemoveContact } from './contact.services';
import { PAGINATE_DEFAULT } from 'constants/common.constants';
import { useEffect, useState } from 'react';
import { ICommonParamsReq } from 'types/common.types';
import { IContact, IContactParams } from './contact.types';
import { ResponseCode } from 'utils/api/api.types';
import { Form, FormInstance } from 'antd';
import { handleRemoveItem } from 'utils/common.utils';

interface Utils {
  // DELETE ITEM
  showModalConfirm: boolean;

  loadingList: boolean;
  data: IContact[];
  params: ICommonParamsReq;
  total: number;
  formFilter: FormInstance<any>;
  onSubmitFilter: () => void;
  onShowModalConfirmDelete: (item: IContact) => void;
  onChangePage: (page: number) => void;
  onSubmitDeleteItem: () => void;
  onCancelModalConfirm: () => void;
}

export default function ContactUtils(): Utils {
  const [loadingList, setLoadingList] = useState<boolean>(false);
  const [data, setData] = useState<IContact[]>([]);
  const [total, setTotal] = useState<number>(0);
  const [formFilter] = Form.useForm();
  const [params, setParams] = useState<IContactParams>({
    limit: PAGINATE_DEFAULT.LIMIT,
    page: PAGINATE_DEFAULT.PAGE
  });

  useEffect(() => {
    fetchListContact(params);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  // fetch list contact
  const fetchListContact = async (params: IContactParams) => {
    try {
      if (loadingList) return;
      setLoadingList(true);
      const res = await apiGetAllContact(params);
      setLoadingList(false);
      if (res.code === ResponseCode.SUCCESS) {
        setData(res?.content || []);
        setTotal(res?.metadata?.total);
      }
    } catch (error) {
      setLoadingList(false);
    }
  };

  // on change page
  const onChangePage = (page: number) => {
    setParams({
      ...params,
      page
    });
  };

  // filter contact by name and phone
  const onSubmitFilter = () => {
    formFilter.validateFields().then(async (values) => {
      setParams({
        ...params,
        name: values?.name ? values?.name.trim() : undefined,
        phone: values?.phone ? values?.phone.trim() : undefined
      });
    });
  };

  // DELETE ITEM
  const [showModalConfirm, setShowModalConfirm] = useState<boolean>(false);
  const [itemNeedDelete, setItemNeedDelete] = useState<IContact>();
  const [loadingDelete, setLoadingDelete] = useState<boolean>(false);

  // on click show modal confirm delete item
  const onShowModalConfirmDelete = (item: IContact) => {
    setItemNeedDelete(item);
    setShowModalConfirm(true);
  };

  // confirm delete item
  const onSubmitDeleteItem = async () => {
    if (!itemNeedDelete || loadingDelete) return;
    try {
      setLoadingDelete(true);
      const res = await apiRemoveContact(itemNeedDelete.id);
      setLoadingDelete(false);
      if (res.code === ResponseCode.SUCCESS) {
        // get id item deleted
        const idItemDelete = res?.content?.id;
        if (idItemDelete) {
          const newListData = handleRemoveItem(data, idItemDelete);
          setData(newListData);
        }
        setShowModalConfirm(false);
        setItemNeedDelete(undefined);
      }
    } catch (error) {
      setLoadingDelete(false);
      setShowModalConfirm(false);
      setItemNeedDelete(undefined);
      throw error;
    }
  };

  const onCancelModalConfirm = () => {
    setShowModalConfirm(false);
    setItemNeedDelete(undefined);
  };

  return {
    showModalConfirm,
    onSubmitDeleteItem,
    onShowModalConfirmDelete,
    onCancelModalConfirm,

    loadingList,
    data,
    params,
    total,
    formFilter,
    onChangePage,
    onSubmitFilter
  };
}
