import { Form, FormInstance } from 'antd';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { routePath } from 'routing/path.routing';
import { DataResponseError, ResponseCode } from 'utils/api/api.types';
import { apiCreateContact, apiGetDetailContact, apiUpdateContact } from './contact.services';
import { IBodyContactRes } from './contact.types';

interface Utils {
  form: FormInstance<any>;
  loadingCreateEdit: boolean;

  loadingDetail: boolean;

  contactId?: number;
  onSaveData: () => void;
  onCancelSaveData: () => void;
}

export default function ContactDetailUtils(): Utils {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [loadingCreateEdit, setLoadingCreateEdit] = useState<boolean>(false);

  const [loadingDetail, setLoadingDetail] = useState<boolean>(false);

  const [contactId, setContactId] = useState<number>();

  const params = useParams();

  useEffect(() => {
    const getContactId = params?.id;
    if (getContactId) {
      setContactId(Number(getContactId));
      fetchDetailContact(Number(getContactId));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  // fetch detail contact
  const fetchDetailContact = async (id: number) => {
    try {
      if (loadingDetail) return;
      setLoadingDetail(true);
      const res = await apiGetDetailContact(id);
      setLoadingDetail(false);
      if (res.code === ResponseCode.SUCCESS) {
        form.setFieldsValue({
          name: res?.content?.name || '',
          phone: res?.content?.phone || '',
          address: res?.content?.address || ''
        });
      }
    } catch (error) {
      setLoadingDetail(false);
      throw error;
    }
  };

  const onSaveData = () => {
    if (loadingCreateEdit) return;
    form.validateFields().then(async (values) => {
      try {
        const body: IBodyContactRes = {
          name: values?.name,
          phone: values?.phone,
          address: values?.address
        };

        // check if id valid is edit
        if (contactId) {
          setLoadingCreateEdit(true);
          const res = await apiUpdateContact(contactId, body);
          setLoadingCreateEdit(false);
          if (res.code === ResponseCode.SUCCESS) {
            navigate(routePath.Contact);
          }
        } else {
          setLoadingCreateEdit(true);
          const res = await apiCreateContact(body);
          setLoadingCreateEdit(false);
          if (res.code === ResponseCode.SUCCESS) {
            navigate(routePath.Contact);
          }
        }
      } catch (error: any) {
        const newError = error as DataResponseError;
        setLoadingCreateEdit(false);
        throw newError;
      }
    });
  };

  const onCancelSaveData = () => {
    navigate(-1);
  };

  return {
    form,
    loadingCreateEdit,

    loadingDetail,

    contactId,
    onSaveData,
    onCancelSaveData
  };
}
