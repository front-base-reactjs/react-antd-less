import { ResponseBase } from 'utils/api/api.types';
import ApiUtils from 'utils/api/api.utils';
import { IBodyContactRes, IContact, IContactParams } from './contact.types';

const apiName = {
  contact_list: '/contact/list',
  contact: '/contact'
};

export const apiGetAllContact = async (params?: IContactParams) => {
  const res = await ApiUtils.fetch<IContactParams, ResponseBase<IContact[]>>(
    apiName.contact_list,
    params
  );
  return res;
};

export const apiGetDetailContact = async (id: number) => {
  const res = await ApiUtils.fetch<number, ResponseBase<IContact>>(`${apiName.contact}/${id}`);
  return res;
};

export const apiRemoveContact = async (id: number) => {
  const res = await ApiUtils.remove<number, ResponseBase<{ id: number }>>(
    `${apiName.contact}/${id}`
  );
  return res;
};

export const apiUpdateContact = async (id: number, body: IBodyContactRes) => {
  const res = await ApiUtils.put<IBodyContactRes, ResponseBase<string>>(
    `${apiName.contact}/${id}`,
    body
  );
  return res;
};

export const apiCreateContact = async (body: IBodyContactRes) => {
  const res = await ApiUtils.post<IBodyContactRes, ResponseBase<IContact>>(apiName.contact, body);
  return res;
};
