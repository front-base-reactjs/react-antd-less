import { Card, Col, Form, Row } from 'antd';
import ButtonForm from 'libraries/form/button/button-form';
import InputForm from 'libraries/form/input/input-form';
import ContainerLayout from 'libraries/layouts/container.layout';
import { useTranslation } from 'react-i18next';
import ContactDetailUtils from './utils/contact-detail.utils';

export default function ContactDetailPage() {
  const { t } = useTranslation();
  const {
    form,
    loadingCreateEdit,
    loadingDetail,

    contactId,
    onSaveData,
    onCancelSaveData
  } = ContactDetailUtils();
  return (
    <ContainerLayout
      title={contactId ? t('contact_page.title_edit') : t('contact_page.title_add')}
      titlePage={contactId ? t('contact_page.title_edit') : t('contact_page.title_add')}
      loading={loadingDetail}
    >
      <Form layout="vertical" name="basic" autoComplete="off" form={form}>
        <Card>
          <Row gutter={50}>
            <Col span={12}>
              <InputForm
                label={t('contact_page.name')}
                name="name"
                rules={[
                  {
                    required: true,
                    message: t('messages.errors.require', { field: t('contact_page.name') })
                  }
                ]}
              />
            </Col>
            <Col span={12}>
              <InputForm
                label={t('column_label.phone')}
                name="phone"
                rules={[
                  {
                    required: true,
                    message: t('messages.errors.require', { field: t('column_label.phone') })
                  }
                ]}
              />
            </Col>
            <Col span={12}>
              <InputForm
                label={t('column_label.address')}
                name="address"
                rules={[
                  {
                    required: true,
                    message: t('messages.errors.require', { field: t('column_label.address') })
                  }
                ]}
              />
            </Col>
          </Row>
        </Card>

        <Card style={{ marginTop: 32 }}>
          <ButtonForm
            type="default"
            title={t('cancel')}
            className="mr-12"
            onClick={onCancelSaveData}
          />
          <ButtonForm
            htmlType="submit"
            title={t('save')}
            loading={loadingCreateEdit}
            onClick={onSaveData}
          />
        </Card>
      </Form>
    </ContainerLayout>
  );
}
