import { Card, Col, Form, Row, Table } from 'antd';
import ButtonForm from 'libraries/form/button/button-form';
import ContainerLayout from 'libraries/layouts/container.layout';
import { useTranslation } from 'react-i18next';
import { PlusOutlined } from '@ant-design/icons';
import ContactUtils from './utils/contact.utils';
import { useNavigate } from 'react-router-dom';
import { routePath } from 'routing/path.routing';
import Loading from 'libraries/components/loading';
import ContactColumns from './utils/contact.columns';
import PaginationBase from 'libraries/components/pagination-base';
import ModalConfirm from 'libraries/models/modal-confirm';
import InputForm from 'libraries/form/input/input-form';

export default function ContactPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const {
    loadingList,
    data,
    params,
    total,
    formFilter,
    onChangePage,
    onSubmitFilter,

    // DELETE ITEM
    showModalConfirm,
    onShowModalConfirmDelete,
    onSubmitDeleteItem,
    onCancelModalConfirm
  } = ContactUtils();

  return (
    <ContainerLayout
      title={t('contact_page.title')}
      actions={
        <ButtonForm
          type="default"
          title={t('add')}
          icon={<PlusOutlined />}
          onClick={() => navigate(routePath.ContactAdd)}
        />
      }
      filterActions={
        <Form layout="vertical" name="basic" autoComplete="off" form={formFilter}>
          <Row gutter={30}>
            <Col span={6}>
              <InputForm label={t('contact_page.name')} name="name" allowClear />
            </Col>
            <Col span={6}>
              <InputForm label={t('column_label.phone')} name="phone" allowClear />
            </Col>
            <Col span={6}>
              <div style={{ marginBottom: 30 }}></div>
              <ButtonForm htmlType="submit" title={t('filter')} onClick={onSubmitFilter} />
            </Col>
          </Row>
        </Form>
      }
    >
      <Card>
        <Row>
          <Col span={24}>
            <Table
              loading={loadingList ? { indicator: <Loading /> } : false}
              pagination={false}
              dataSource={data}
              columns={ContactColumns({
                page: params.page,
                onConFirmDelete: onShowModalConfirmDelete,
                pathEdit: routePath.Contact
              })}
              rowKey="id"
            />
          </Col>

          {/** Pagination */}
          {total > params.limit && (
            <PaginationBase page={params.page} total={total} onPageChange={onChangePage} />
          )}
        </Row>
      </Card>

      {/** Modal Confirm Delete */}
      <ModalConfirm
        visible={showModalConfirm}
        onConfirmOk={onSubmitDeleteItem}
        onCancel={onCancelModalConfirm}
      />
    </ContainerLayout>
  );
}
