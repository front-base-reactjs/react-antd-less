import { Button, Checkbox, Col, Input, Row } from 'antd';
import ContainerLayout from 'libraries/layouts/container.layout';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { routePath } from 'routing/path.routing';

export default function AboutPage() {
  const data = [
    'Tao',
    'Le',
    'Oi',
    'Coc',
    'Xoai',
    'Mang cut',
    'Man',
    'Na',
    'Chom Chom',
    'Vai',
    'Buoi',
    'Mang cau',
    'Nho',
    'Chuoi',
    'Sung',
    'Nhan'
  ]; // data

  const [listCheckBoxDisplay, setListCheckBoxDisplay] = useState<string[]>(data);

  const [listChecked, setListChecked] = useState<string[]>([]);

  const [valueSearch, setValueSearch] = useState<string>('');

  const onSearch = (values: any) => {
    setValueSearch(values.target.value);
  };

  const onSubmitSearch = () => {
    const newList = [...data];
    const listSearch = newList.filter((x) => x.toUpperCase().includes(valueSearch.toUpperCase()));
    setListCheckBoxDisplay(listSearch);
  };

  const onChangeCheckBox = (values: any[]) => {
    const notCheckedBoxDisplay = listCheckBoxDisplay.filter((item) => !values.includes(item));
    const oldChecked = listChecked.filter(
      (item) => !notCheckedBoxDisplay.includes(item) && !values.includes(item)
    );
    const newValue = [...oldChecked, ...values];
    setListChecked(newValue);
  };

  return (
    <ContainerLayout title="About">
      <Link to={`${routePath.About}/12`}>About Detail</Link>

      <div style={{ display: 'flex', maxWidth: 200, marginTop: 30 }}>
        <Input placeholder="Search" onChange={onSearch} />
        <Button style={{ marginLeft: 30 }} onClick={onSubmitSearch}>
          Search
        </Button>
      </div>

      <Checkbox.Group value={listChecked} onChange={onChangeCheckBox}>
        <Row style={{ marginTop: 30 }} gutter={[30, 30]}>
          {listCheckBoxDisplay.map((item) => {
            return (
              <Col span={4} key={item}>
                <Checkbox value={item}>{item}</Checkbox>
              </Col>
            );
          })}
        </Row>

        <h1 style={{ marginTop: 30 }}>List Checked</h1>
        <Row gutter={30}>
          {listChecked.map((item) => {
            return (
              <Col span={3} key={item}>
                <Checkbox disabled={true} value={item}>
                  {item}
                </Checkbox>
              </Col>
            );
          })}
        </Row>
      </Checkbox.Group>
    </ContainerLayout>
  );
}
