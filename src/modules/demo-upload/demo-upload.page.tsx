import { Card, Col, Form, Row, Input, message, Image } from 'antd';
import Loading from 'libraries/components/loading';
import ButtonForm from 'libraries/form/button/button-form';
import ContainerLayout from 'libraries/layouts/container.layout';
import { ChangeEvent, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { apiGetSignUrlUpload, apiUploadFile } from './utils/demo-upload.services';
import { fallBackImage } from '../../constants/common.constants';

export default function DemoUploadPage() {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [file, setFile] = useState<File>();
  const [loading, setLoading] = useState<boolean>(false);
  const [url, setUrl] = useState<string>(fallBackImage);

  const onChangeFile = (values: ChangeEvent<HTMLInputElement>) => {
    const files = values.target.files;
    if (!files || files.length <= 0) return message.error('File must be require.');
    setFile(files[0]);
  };

  const onSaveData = () => {
    if (loading) return;
    form.validateFields().then(async (values) => {
      setLoading(true);
      if (!values.file || !file) return message.error('File must be require.');
      const signUrl = await apiGetSignUrlUpload({
        fileName: file?.name || '',
        folderName: 'test'
      });
      const formData = new FormData();
      formData.append('file', file);
      const res = await apiUploadFile(signUrl.url, formData);
      setLoading(false);
      setUrl(res.url);
    });
  };

  return (
    <ContainerLayout title="Demo Upload">
      <Form layout="vertical" name="basic" autoComplete="off" form={form}>
        <Card>
          <Row gutter={50}>
            <Col span={12}>
              <Form.Item
                label="File upload"
                name="file"
                rules={[
                  {
                    required: true,
                    message: t('messages.errors.require', { field: t('contact_page.name') })
                  }
                ]}
              >
                <Input
                  allowClear
                  size="large"
                  placeholder="Upload file"
                  type="file"
                  onChange={onChangeFile}
                />
              </Form.Item>
            </Col>
          </Row>
        </Card>

        <Card style={{ marginTop: 32, width: 'auto' }}>
          <span style={{ position: "relative", display: "inline-block", maxWidth: 200 }}>
            {loading && <span style={{ position: "absolute", top: "50%", right: "50%", transform: "translate(50%,-50%)", zIndex: 1 }}><Loading /></span>}
            <Image fallback={fallBackImage} loading="lazy" style={{ width: '100%', maxHeight: '100%' }} src={url} alt="image" />
          </span>
        </Card>

        <Card style={{ marginTop: 32 }}>
          <ButtonForm htmlType="submit" title={t('save')} loading={false} onClick={onSaveData} />
        </Card>
      </Form>
    </ContainerLayout>
  );
}
