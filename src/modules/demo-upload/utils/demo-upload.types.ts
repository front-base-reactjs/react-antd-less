export interface ICloudinaryRes {
  url: string;
}

export interface ICloudinaryParams {
  fileName: string;
  folderName?: string;
}
