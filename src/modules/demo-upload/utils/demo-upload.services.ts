import ApiUtils from 'utils/api/api.utils';
import { ICloudinaryParams, ICloudinaryRes } from './demo-upload.types';
import axios from 'axios';

const apiName = {
  signUrlUpload: 'cloudinary/sign-url'
};

export const apiGetSignUrlUpload = async (params: ICloudinaryParams) => {
  const res = await ApiUtils.fetch<ICloudinaryParams, ICloudinaryRes>(
    apiName.signUrlUpload,
    params
  );
  return res;
};

export const apiUploadFile = async (url: string, data: FormData) => {
  const res = await axios.post(url, data);
  return res.data;
};
