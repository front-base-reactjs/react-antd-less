import { ITokenInfo } from 'types/common.types';
import { ResponseBase } from 'utils/api/api.types';
import ApiUtils from 'utils/api/api.utils';
import { ISignInReq } from './sign-in.types';

const apiName = {
  sign_in: '/account/sign-in',
  sing_up: '/account/sign-up'
};

export const apiSignIn = async (body: ISignInReq) => {
  const res = await ApiUtils.post<ISignInReq, ResponseBase<ITokenInfo>>(apiName.sign_in, body);
  return res;
};
