export interface ISignInReq {
  username: string;
  password: string;
  systemType?: string;
}
