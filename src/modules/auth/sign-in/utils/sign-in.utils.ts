import { Form, FormInstance } from 'antd';
import useAuth from 'hooks/redux/auth/useAuth';
import { useState, useEffect } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { routePath } from 'routing/path.routing';
import { ISystemType } from 'types/common.types';
import { ResponseCode, DataResponseError } from 'utils/api/api.types';
import { handleErrors } from 'utils/common.utils';
import { apiSignIn } from './sign-in.services';
import { ISignInReq } from './sign-in.types';

interface Utils {
  formLogIn: FormInstance<any>;
  loadingSignIn: boolean;
  onLogin: () => void;
  onGoToRegister: () => void;
  onGoToForgotPassword: () => void;
}

export default function SignInUtils(): Utils {
  const navigate = useNavigate();
  const [formLogIn] = Form.useForm();
  const { setAuth } = useAuth();

  const [loadingSignIn, setLoadingSignIn] = useState<boolean>(false);
  const [searchParams] = useSearchParams();
  const userNameRegistered = searchParams.get('username');

  useEffect(() => {
    // check if user registered will fill username into form input
    if (userNameRegistered) {
      formLogIn.setFieldValue('username', userNameRegistered);
    }
  }, [formLogIn, userNameRegistered]);

  // when on click button submit login
  const onLogin = () => {
    formLogIn.validateFields().then(async (values: any) => {
      try {
        if (loadingSignIn) return;
        setLoadingSignIn(true);
        const body: ISignInReq = {
          username: values?.username,
          password: values?.password,
          systemType: ISystemType.CMS_SYSTEM
        };
        const res = await apiSignIn(body);
        if (res.code === ResponseCode.SUCCESS) {
          setAuth({
            tokenInfo: res.content,
            user: {
              username: values?.username
            }
          });
        }

        setLoadingSignIn(false);
      } catch (error: any) {
        const newError = error as DataResponseError;
        handleErrors(formLogIn, newError);
        setLoadingSignIn(false);
      }
    });
  };

  // go to register page
  const onGoToRegister = () => {
    navigate(routePath.SignUp);
  };

  // go to forgot password page
  const onGoToForgotPassword = () => {
    navigate(routePath.ForgotPassword);
  };

  return {
    formLogIn,
    loadingSignIn,
    onLogin,
    onGoToRegister,
    onGoToForgotPassword
  };
}
