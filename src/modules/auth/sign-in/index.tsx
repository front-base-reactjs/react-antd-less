import { Form } from 'antd';
import ButtonForm from 'libraries/form/button/button-form';
import InputForm from 'libraries/form/input/input-form';
import InputPasswordForm from 'libraries/form/input/input-password-form';
import ContainerLayout from 'libraries/layouts/container.layout';
import { useTranslation } from 'react-i18next';
import SignInUtils from './utils/sign-in.utils';
import clsx from 'clsx';
import useTheme from 'hooks/redux/theme/useTheme';

export default function SignInPage() {
  const { t } = useTranslation();
  const { formLogIn, loadingSignIn, onLogin, onGoToRegister, onGoToForgotPassword } = SignInUtils();
  const { isDark } = useTheme();

  return (
    <ContainerLayout title={t('sign_in.title')}>
      <div
        className={clsx('sign-in-page', {
          'dark-theme-custom': isDark,
          'light-theme-custom': !isDark,
          'dark-shadow': isDark,
          'light-shadow': !isDark,
          'dark-border': isDark,
          'light-border': !isDark
        })}
      >
        <h1 className={clsx('sign-in-page-title')}>{t('sign_in.title')}</h1>
        <Form form={formLogIn} layout="vertical" name="basic" autoComplete="off">
          <div>
            <InputForm
              label={t('username')}
              name="username"
              rules={[
                {
                  required: true,
                  message: t('messages.errors.require', { field: t('username') })
                }
              ]}
            />
          </div>

          <div>
            <InputPasswordForm
              label={t('password')}
              name="password"
              rules={[
                {
                  required: true,
                  message: t('messages.errors.require', { field: t('password') })
                }
              ]}
            />
          </div>

          {/** Action more */}
          <div className={clsx('mb-44')}>
            <div className="sign-in-page-action-forgot">
              <span>{t('don_have_account')}</span>
              <span className="link link-custom" onClick={onGoToRegister}>
                {t('create_an_account')}
              </span>
            </div>
            <span className="link" onClick={onGoToForgotPassword}>
              {t('forgot_password')}
            </span>
          </div>

          <Form.Item>
            <ButtonForm
              title={t('login_button')}
              htmlType="submit"
              buttonType="light"
              className="sign-in-page-submit"
              onClick={onLogin}
              loading={loadingSignIn}
            />
          </Form.Item>
        </Form>
      </div>
    </ContainerLayout>
  );
}
