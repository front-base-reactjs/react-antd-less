import { IRoleType } from 'types/common.types';

export interface ISignUpReq {
  username: string;
  email: string;
  password: string;
  role?: IRoleType;
}

export interface ISignUpRes {
  createdAt: string;
  deletedAt?: string;
  email: string;
  id: string;
  password: string;
  role: IRoleType;
  updatedAt: string;
  username: string;
}
