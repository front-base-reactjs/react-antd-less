import { Form, FormInstance } from 'antd';
import { useState } from 'react';
import { createSearchParams, useNavigate } from 'react-router-dom';
import { DataResponseError, ResponseCode } from 'utils/api/api.types';
import { handleErrors } from 'utils/common.utils';
import { apiSignUp } from './sign-up.services';
import { ISignUpReq } from './sign-up.types';
import { routePath } from 'routing/path.routing';
import { IRoleType } from 'types/common.types';

interface Utils {
  formSignUp: FormInstance<any>;
  loadingSignUp: boolean;
  onSignUp: () => void;
  onGoToLogin: () => void;
}

export default function SignUpUtils(): Utils {
  const [formSignUp] = Form.useForm();
  const [loadingSignUp, setLoadingSignUp] = useState<boolean>(false);
  const navigate = useNavigate();

  // when click button sign up will send a request server create new account
  const onSignUp = () => {
    formSignUp.validateFields().then(async (values: any) => {
      try {
        if (loadingSignUp) return;
        setLoadingSignUp(true);
        const body: ISignUpReq = {
          username: values?.username,
          password: values?.password,
          email: values?.email,
          role: IRoleType.SUPPER_ADMIN
        };

        const res = await apiSignUp(body);
        setLoadingSignUp(false);

        if (res.code === ResponseCode.SUCCESS) {
          const content = res?.content;
          navigate({
            pathname: routePath.SignIn,
            search: createSearchParams({
              username: content?.username
            }).toString()
          });
        }
      } catch (error: any) {
        const newError = error as DataResponseError;
        handleErrors(formSignUp, newError);
        setLoadingSignUp(false);
      }
    });
  };

  // when click button go to login will redirect to login page
  const onGoToLogin = () => {
    navigate(routePath.SignIn);
  };

  return {
    formSignUp,
    loadingSignUp,
    onGoToLogin,
    onSignUp
  };
}
