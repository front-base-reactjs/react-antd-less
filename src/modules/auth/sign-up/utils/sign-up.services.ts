import { ResponseBase } from 'utils/api/api.types';
import ApiUtils from 'utils/api/api.utils';
import { ISignUpReq, ISignUpRes } from './sign-up.types';

const apiName = {
  sing_up: '/account/sign-up'
};

export const apiSignUp = async (body: ISignUpReq) => {
  const res = await ApiUtils.post<ISignUpReq, ResponseBase<ISignUpRes>>(apiName.sing_up, body);
  return res;
};
