import { Form } from 'antd';
import clsx from 'clsx';
import useTheme from 'hooks/redux/theme/useTheme';
import ButtonForm from 'libraries/form/button/button-form';
import InputForm from 'libraries/form/input/input-form';
import InputPasswordForm from 'libraries/form/input/input-password-form';
import ContainerLayout from 'libraries/layouts/container.layout';
import { useTranslation } from 'react-i18next';
import SignUpUtils from './utils/sign-up.utils';

export default function SignUpPage() {
  const { t } = useTranslation();
  const { isDark } = useTheme();
  const { formSignUp, loadingSignUp, onSignUp, onGoToLogin } = SignUpUtils();

  return (
    <ContainerLayout title={t('sign_up.title')}>
      <div
        className={clsx('sign-in-page', {
          'dark-theme-custom': isDark,
          'light-theme-custom': !isDark,
          'dark-shadow': isDark,
          'light-shadow': !isDark,
          'dark-border': isDark,
          'light-border': !isDark
        })}
      >
        <h1 className={clsx('sign-in-page-title')}>{t('sign_up.title')}</h1>
        <Form form={formSignUp} layout="vertical" name="basic" autoComplete="off">
          <div>
            <InputForm
              label={t('username')}
              name="username"
              rules={[
                {
                  required: true,
                  message: t('messages.errors.require', { field: t('username') })
                }
              ]}
            />
          </div>

          <div>
            <InputForm
              label={t('email_address')}
              name="email"
              rules={[
                {
                  required: true,
                  message: t('messages.errors.require', { field: t('email_address') })
                },

                {
                  type: 'email',
                  message: t('messages.errors.email_invalid')
                }
              ]}
            />
          </div>

          <div>
            <InputPasswordForm
              label={t('password')}
              name="password"
              rules={[
                {
                  required: true,
                  message: t('messages.errors.require', { field: t('password') })
                }
              ]}
            />
          </div>

          {/** Action more */}
          <div className={clsx('mb-44')}>
            <div className="sign-in-page-action-forgot">
              <span>{t('have_an_account')}</span>
              <span className="link link-custom" onClick={onGoToLogin}>
                {t('sign_in.title')}
              </span>
            </div>
          </div>

          <Form.Item>
            <ButtonForm
              title={t('login_button')}
              htmlType="submit"
              buttonType="light"
              className="sign-in-page-submit"
              onClick={onSignUp}
              loading={loadingSignUp}
            />
          </Form.Item>
        </Form>
      </div>
    </ContainerLayout>
  );
}
