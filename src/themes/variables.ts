const themeVariables = {
  '@primary-color': '#ae18ff',
  '@layout-header-background': '#fff',
  '@border-radius-base': '8px'
};
export default themeVariables;
