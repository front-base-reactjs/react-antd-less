import useInitSubject from './rxjs/useInitSubject';
import useInitSocketIO from './socket-io/useInitSocketIO';
import useInitDocument from './redux/document/useInitDocument';
import useInitTheme from './redux/theme/useInitTheme';
import useInitLocale from './redux/locale/useInitLocale';

export default function useInitBase() {
  useInitSubject();
  useInitSocketIO();
  useInitDocument();
  useInitTheme();
  useInitLocale();
}
