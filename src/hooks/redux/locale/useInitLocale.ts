import moment from 'moment';
import { useEffect } from 'react';
import useLocale from './useLocale';
import i18n from 'i18next';
import localStorageUtils, { KeyStorage } from 'utils/local-storage.utils';

function useInitLocale() {
  const { locale } = useLocale();

  useEffect(() => {
    moment.locale(locale.locale);
    i18n.changeLanguage(locale.locale);
    window.document.body.dataset.locale = locale.locale;
    localStorageUtils.setObject(KeyStorage.LOCALE, {
      ...locale
    });
  }, [locale]);
}

export default useInitLocale;
