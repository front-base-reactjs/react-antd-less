import { changeLocale, locales } from 'hooks/redux/locale/reducer';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'utils/redux-store';

export type LocaleName = keyof typeof locales;

const langs: {
  value: LocaleName;
  label: string;
}[] = [];

for (const [key, value] of Object.entries(locales)) {
  langs.push({ value: key as LocaleName, label: value.name });
}

function useLocale() {
  const locale = useSelector((state: RootState) => state.locale);
  const dispatch = useDispatch();

  const setLocale = (newLocale: LocaleName) => {
    const actionChangeLocale = changeLocale(locales[newLocale].locale);
    dispatch(actionChangeLocale);
  };

  return { locale, langs, setLocale };
}

export default useLocale;
