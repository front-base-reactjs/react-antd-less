import auth from './auth/reducer';
import document from './document/reducer';
import theme from './theme/reducer';
import locale from './locale/reducer';

const baseReducer = {
  auth,
  document,
  theme,
  locale
};

export default baseReducer;
