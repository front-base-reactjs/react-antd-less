import { useEffect } from 'react';
import themes from 'themes';
import useTheme from './useTheme';
import localStorageUtils, { KeyStorage } from 'utils/local-storage.utils';

function useInitTheme() {
  const { theme } = useTheme();

  useEffect(() => {
    const newVariables = { ...theme.variables };
    const configTheme = {
      ...themes[theme.name],
      ...newVariables
    };
    global.less
      .modifyVars(configTheme)
      .then(() => {
        localStorageUtils.setObject(KeyStorage.THEME, theme);
      })
      .catch((err: any) => console.log(err));
  }, [theme]);
}

export default useInitTheme;
