export const routePath = {
  HomePage: '/',
  About: '/about',
  AboutDetail: '/about/:id',
  Contact: '/contact',
  ContactAdd: '/contact/add',
  Setting: '/setting',
  DemoUpload: '/demo-upload',

  // Auth Path
  Auth: '/auth',
  SignIn: '/auth/sign-in',
  SignUp: '/auth/sign-up',
  ForgotPassword: '/auth/forgot-password'
};
