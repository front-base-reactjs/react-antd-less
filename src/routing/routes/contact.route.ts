import { routePath } from 'routing/path.routing';
import { lazy } from 'react';
import { ContactsOutlined } from '@ant-design/icons';
import React from 'react';
import { IRouter } from 'routing/config.routing';

export const ContactRoute: IRouter[] = [
  {
    path: routePath.Contact,
    element: lazy(() => import('modules/contact/contact.page')),
    icons: React.createElement(ContactsOutlined),
    name: 'Contact'
  },
  {
    path: `${routePath.Contact}/:id`,
    element: lazy(() => import('modules/contact/contact-detail.page')),
    name: 'Contact detail',
    hiddenMenu: true
  },
  {
    path: routePath.ContactAdd,
    element: lazy(() => import('modules/contact/contact-detail.page')),
    name: 'Contact new',
    hiddenMenu: true
  }
];
