// Layout
import { lazy } from 'react';
import { routePath } from './path.routing';
import { HomeOutlined, QuestionCircleOutlined, UploadOutlined } from '@ant-design/icons';
import React from 'react';
import { ContactRoute } from './routes/contact.route';

export interface IRouter {
  icons?: JSX.Element | string | any;
  path: string;
  element: any;
  children?: IRouter[];
  name?: string;

  // status
  haveChild?: boolean;
  isAuth?: boolean;
  hiddenMenu?: boolean;
}

const configRoutes: IRouter[] = [
  // Main Page
  {
    path: routePath.HomePage,
    element: lazy(() => import('libraries/layouts/main.layout')),
    icons: React.createElement(HomeOutlined),
    name: 'Home',
    hiddenMenu: true,
    isAuth: true,
    children: [
      {
        path: routePath.About,
        element: lazy(() => import('modules/about')),
        icons: React.createElement(QuestionCircleOutlined),
        name: 'About',
        haveChild: true,
        children: [
          {
            path: routePath.About,
            element: lazy(() => import('modules/about')),
            name: 'About'
          }
        ]
      },
      {
        path: routePath.AboutDetail,
        element: lazy(() => import('modules/about/about-detail')),
        name: 'About detail',
        hiddenMenu: true
      },
      {
        path: routePath.DemoUpload,
        icons: React.createElement(UploadOutlined),
        element: lazy(() => import('modules/demo-upload/demo-upload.page')),
        name: 'Demo upload'
      },

      // Insert Module
      ...ContactRoute,

      // Not Found
      {
        path: '*',
        element: lazy(() => import('modules/not-found')),
        hiddenMenu: true
      }
    ]
  },

  // Auth
  {
    path: routePath.Auth,
    element: lazy(() => import('libraries/layouts/auth.layout')),
    isAuth: false,
    children: [
      {
        path: routePath.SignIn,
        element: lazy(() => import('modules/auth/sign-in'))
      },
      {
        path: routePath.SignUp,
        element: lazy(() => import('modules/auth/sign-up'))
      },
      {
        path: routePath.ForgotPassword,
        element: lazy(() => import('modules/auth/forgot-password'))
      }
    ]
  },

  // Not Found
  {
    path: '*',
    element: lazy(() => import('modules/not-found'))
  }
];

export default configRoutes;
