import { FormInstance } from 'antd';
import { DataResponseError, ErrorObject, IErrorType } from './api/api.types';
import i18n from './i18n';
import { message } from 'antd';

export const handleErrors = (form: FormInstance<any>, errors: DataResponseError) => {
  const content = errors?.content;
  const errorType = content?.errorType;
  // if error type is business will show toast
  if (errorType === IErrorType.BUSINESS)
    return message.error(i18n.t(`messages.errorsKey.${content?.messageContent}`));

  const constraintsError: ErrorObject[] = content?.constraints || [];
  if (!form || !constraintsError || constraintsError.length <= 0) return;
  constraintsError.forEach((item) => {
    const messages = item?.message || [];
    const property = item?.property;
    const messageConvertI18 = messages.map((message) => i18n.t(`messages.errorsKey.${message}`));

    if (!messages || messages.length <= 0) return;
    form.setFields([
      {
        name: property,
        errors: messageConvertI18
      }
    ]);
  });
};

export const handleRemoveItem = (arr: Array<any>, idItemDeleted: number) => {
  const newArr = [...arr];
  if (!arr || !idItemDeleted) return [];
  const indexItemDeleted = newArr.findIndex((x) => Number(x.id) === Number(idItemDeleted));
  if (indexItemDeleted === -1) return [];
  newArr.splice(indexItemDeleted, 1);
  return newArr;
};
