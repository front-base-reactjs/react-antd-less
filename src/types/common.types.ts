export interface IUser {
  email?: string;
  username: string;
  role?: IRoleType;
}

export enum IRoleType {
  ADMIN = 'ADMIN',
  SUPPER_ADMIN = 'SUPPER_ADMIN',
  END_USER = 'END_USER'
}

export enum IOrderType {
  DESC = 'DESC',
  ASC = 'ASC'
}

export enum ISystemType {
  CMS_SYSTEM = 'CMS_SYSTEM',
  CLIENT_SYSTEM = 'CLIENT_SYSTEM'
}

export interface ITokenInfo {
  accessToken: string;
  refreshToken: string;
  expireTime: number;
}

export interface ICommonParamsReq {
  limit: number;
  page: number;
}
