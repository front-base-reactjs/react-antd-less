import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { Avatar, Button } from 'antd';
import useTheme from 'hooks/redux/theme/useTheme';
import ButtonForm from 'libraries/form/button/button-form';
import { useTranslation } from 'react-i18next';
import { IUser } from 'types/common.types';
import clsx from 'clsx';

interface Props {
  user?: IUser;
  showSider: boolean;
  toggleSider: () => void;
  onLogout: () => void;
}

export default function MainHeader({ showSider, user, toggleSider, onLogout }: Props) {
  const { t } = useTranslation();
  const { isDark } = useTheme();

  return (
    <div className="header-main-layout">
      <div>
        <Button
          type="link"
          icon={showSider ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
          onClick={() => toggleSider && toggleSider()}
        />
      </div>
      <p className="welcome-text">
        {t('welcome')}
        <span className="link" style={{ marginLeft: 5 }}>
          {user?.username}
        </span>
      </p>
      <div className="avatar-profile">
        <Avatar
          className={clsx('dark-border', {
            'dark-shadow': isDark
          })}
          size={40}
          src="https://joeschmoe.io/api/v1/random"
          style={{ marginRight: 15 }}
        />
        <ButtonForm onClick={onLogout} />
      </div>
    </div>
  );
}
