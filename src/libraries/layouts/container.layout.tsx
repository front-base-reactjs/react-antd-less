import { Card } from 'antd';
import useDocument from 'hooks/redux/document/useDocument';
import Loading from 'libraries/components/loading';
import { useEffect } from 'react';

interface Props {
  title?: string;
  titlePage?: string;
  children?: any;
  loading?: boolean;
  actions?: React.ReactNode | React.ReactNodeArray;
  filterActions?: React.ReactNode | React.ReactNodeArray;
}

export default function ContainerLayout({
  children,
  title = process.env.REACT_APP_NAME || 'REACT-BASE',
  actions,
  filterActions,
  loading = false,
  titlePage
}: Props) {
  const { setTitle } = useDocument();
  useEffect(() => {
    setTitle(title);
  }, [setTitle, title]);

  return (
    <div className="relative">
      {(actions || filterActions) && (
        <Card className="header-container-layout" style={{ marginBottom: 32 }}>
          <div className="flex-1">{filterActions}</div>
          <div>{actions}</div>
        </Card>
      )}
      {titlePage && <h1 style={{ fontSize: 18, fontWeight: 'bold' }}>{titlePage}</h1>}
      <div>
        {loading && <Loading />}
        {children}
      </div>
    </div>
  );
}
