export default function AuthFooter() {
  return (
    <div>
      <p className="flex item-center justify-center">
        &copy; repo{' '}
        <a href="https://gitlab.com/vungbt1999" target="_blank" rel="noreferrer" className="ml-5">
          vungbt1999
        </a>
      </p>
    </div>
  );
}
