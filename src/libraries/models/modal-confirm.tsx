import { Modal } from 'antd';
import { useTranslation } from 'react-i18next';

interface Props {
  titleModal?: string;
  visible: boolean;
  onConfirmOk?: () => void;
  onCancel?: () => void;
}
export default function ModalConfirm({ visible, titleModal, onConfirmOk, onCancel }: Props) {
  const { t } = useTranslation();
  return (
    <Modal
      title={titleModal || t('messages.message_modal_confirm')}
      visible={visible}
      onOk={onConfirmOk}
      onCancel={onCancel}
    >
      <p>Some contents...</p>
      <p>Some contents...</p>
      <p>Some contents...</p>
    </Modal>
  );
}
