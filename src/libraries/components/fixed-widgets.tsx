import { SettingOutlined } from '@ant-design/icons';
import { Drawer } from 'antd';
import { Fragment, useState } from 'react';
import { useTranslation } from 'react-i18next';
import CircleColorPicker from './circle-color-picker';
import { Switch } from 'antd';
import useTheme from 'hooks/redux/theme/useTheme';
import { ColorResult } from 'react-color';
import useLocale from 'hooks/redux/locale/useLocale';

export default function FixedWidgets() {
  const [showDrawerSetting, setShowDrawerSetting] = useState<boolean>(false);
  const { t } = useTranslation();
  const { theme, setTheme } = useTheme();
  const { locale, setLocale } = useLocale();
  const primaryColor = theme.variables && theme.variables['@primary-color'];

  return (
    <Fragment>
      <Drawer
        title={t('change_theme')}
        placement="right"
        onClose={() => setShowDrawerSetting(false)}
        visible={showDrawerSetting}
      >
        {/** Change Locale */}
        <div className="mb-16">
          <Switch
            checkedChildren={<span>{t('en')}</span>}
            unCheckedChildren={<span>{t('vi')}</span>}
            onChange={(e) => setLocale(e ? 'en' : 'vi')}
            // checked={langs?. === 'en' ? true : false}
            checked={locale?.locale === 'en' ? true : false}
          />
        </div>

        {/** Dark mode */}
        <div className="mb-16">
          <Switch
            checkedChildren={<span>{t('light')}</span>}
            unCheckedChildren={<span>{t('dark')}</span>}
            onChange={(e) => setTheme({ name: e ? 'default' : 'dark' })}
            checked={theme.name === 'default' ? true : false}
          />
        </div>

        <CircleColorPicker
          width="auto"
          onChange={(color: ColorResult) => {
            setTheme({ variables: { '@primary-color': color.hex } });
          }}
          color={primaryColor}
        />
      </Drawer>
      <div
        className="fixed-widget"
        style={{
          backgroundColor: `${primaryColor}`,
          backgroundImage: `linear-gradient(90deg, ${primaryColor} 0%, #80d0c7 100%)`
        }}
        onClick={() => setShowDrawerSetting(true)}
      >
        <SettingOutlined style={{ fontSize: 20 }} />
      </div>
    </Fragment>
  );
}
