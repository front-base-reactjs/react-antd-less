import { Col, Pagination } from 'antd';
import { PAGINATE_DEFAULT } from 'constants/common.constants';

interface Props {
  page: number;
  total: number;
  limit?: number;
  onPageChange: (page: number) => void;
}

export default function PaginationBase({
  page,
  total,
  limit = PAGINATE_DEFAULT.LIMIT,
  onPageChange
}: Props) {
  return (
    <Col
      span={24}
      style={{
        display: 'inline-flex',
        justifyContent: 'flex-end',
        marginTop: 24
      }}
    >
      <Pagination
        showSizeChanger={false}
        total={total}
        showTotal={(total, range) => `${range[0]}-${range[1]}/${total}`}
        current={page}
        pageSize={limit}
        defaultPageSize={limit}
        onChange={(page) => onPageChange(page)}
      />
    </Col>
  );
}
