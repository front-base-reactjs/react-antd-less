import { Button } from 'antd';
import { BaseButtonProps } from 'antd/es/button/button';
import clsx from 'clsx';
import { ReactNode } from 'react';
import { useTranslation } from 'react-i18next';

interface Props extends BaseButtonProps {
  title?: string;
  loading?: boolean;
  htmlType?: 'button' | 'submit' | 'reset';
  size?: 'large' | 'middle' | 'small';
  type?: 'primary' | 'default' | 'dashed';
  icon?: ReactNode;
  className?: any;
  buttonType?: 'light' | 'dark';
  onClick?: () => void;
}

export default function ButtonForm({
  title,
  loading = false,
  htmlType = 'button',
  type = 'primary',
  size = 'large',
  icon,
  className,
  onClick
}: Props) {
  const { t } = useTranslation();
  return (
    <Button
      className={clsx('ant-btn-primary', {
        'ant-btn-primary-default': type === 'default',
        [className]: true
      })}
      onClick={onClick}
      loading={loading}
      htmlType={htmlType}
      size={size}
      icon={icon}
      type={type}
    >
      {title || t('logout')}
    </Button>
  );
}
