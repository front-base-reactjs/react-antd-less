import { useTranslation } from 'react-i18next';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';

interface Props {
  onEdit?: () => void;
  onConfirmDelete?: () => void;
}
export default function ActionsButton({ onEdit, onConfirmDelete }: Props) {
  const { t } = useTranslation();
  return (
    <div>
      {onEdit && (
        <Tooltip placement="top" title={t('edit')}>
          <EditOutlined className="icon-edit mr-12" onClick={onEdit} />
        </Tooltip>
      )}

      {onConfirmDelete && (
        <Tooltip placement="top" title={t('delete')}>
          <DeleteOutlined className="icon-delete" onClick={onConfirmDelete} />
        </Tooltip>
      )}
    </div>
  );
}
