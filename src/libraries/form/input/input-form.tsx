import { Form, Input } from 'antd';
import { Rule } from 'antd/lib/form';

interface Props {
  label?: string;
  name: string;
  rules?: Rule[];
  placeholder?: string;
  allowClear?: boolean;
  autoComplete?: 'on' | 'off';

  // custom
  classNameFormInput?: any;
  className?: any;
}
export default function InputForm({
  label,
  placeholder,
  name,
  rules,
  allowClear = true,
  autoComplete = 'off'
}: // classNameFormInput,
// className
Props) {
  return (
    <Form.Item label={label} name={name} rules={rules}>
      <Input
        allowClear={allowClear}
        size="large"
        placeholder={placeholder}
        autoComplete={autoComplete}
      />
    </Form.Item>
  );
}
