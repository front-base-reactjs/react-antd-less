import { Form, Input } from 'antd';
import { Rule } from 'antd/lib/form';

interface Props {
  label?: string;
  name: string;
  rules?: Rule[];
  placeholder?: string;
  autoComplete?: 'on' | 'off';
  allowClear?: boolean;

  // custom
  classNameFormInput?: any;
  className?: any;
}
export default function InputPasswordForm({
  label,
  placeholder,
  name,
  rules,
  autoComplete = 'off',
  allowClear = true
}: // classNameFormInput,
// className
Props) {
  return (
    <Form.Item label={label} name={name} rules={rules}>
      <Input.Password
        size="large"
        placeholder={placeholder}
        autoComplete={autoComplete}
        allowClear={allowClear}
      />
    </Form.Item>
  );
}
